package com.gmail.victorkusov.mealbusket.view.activity

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface Navigator {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMealDetails()
}
