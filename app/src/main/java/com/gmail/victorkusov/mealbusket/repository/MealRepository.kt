package com.gmail.victorkusov.mealbusket.repository

import com.gmail.victorkusov.mealbusket.model.Product
import io.reactivex.Single

interface MealRepository {

    fun getMealList(): Single<ArrayList<Product>>

    fun getMeal(): Single<Product>

    fun setSelectedItemId(id: String)

}