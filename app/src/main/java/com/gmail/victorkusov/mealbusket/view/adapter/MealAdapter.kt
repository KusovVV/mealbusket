package com.gmail.victorkusov.mealbusket.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.gmail.victorkusov.mealbusket.R
import com.gmail.victorkusov.mealbusket.model.Product
import kotlinx.android.synthetic.main.item_meal.view.*

class MealAdapter(
    private val listener: ItemClickListener
) :
    RecyclerView.Adapter<MealAdapter.Holder>(),
    AdapterController {

    private val data = ArrayList<Product>()

    private val clickListener = View.OnClickListener { listener.onItemClick(it.tag as String) }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        Holder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_meal,
                parent,
                false
            )
        )

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: Holder, position: Int) = holder.bind(data[position])

    override fun onViewAttachedToWindow(holder: Holder) {
        super.onViewAttachedToWindow(holder)
        holder.itemView.setOnClickListener(clickListener)
    }


    override fun onViewDetachedFromWindow(holder: Holder) {
        holder.itemView.setOnClickListener(null)
        super.onViewDetachedFromWindow(holder)
    }

    override fun setNewData(data: ArrayList<Product>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val transforms = RequestOptions().transforms(
            CenterInside(),
            RoundedCorners(16)
        )

        fun bind(item: Product) {
            itemView.txtTitle.text = item.name
            itemView.txtPrice.text = item.price.toString()

            Glide.with(itemView.context)
                .load(item.image)
                .apply(transforms)
                .into(itemView.imgIcon)

            itemView.tag = item.id
        }
    }
}
