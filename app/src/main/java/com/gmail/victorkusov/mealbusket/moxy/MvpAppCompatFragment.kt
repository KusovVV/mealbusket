package com.gmail.victorkusov.mealbusket.moxy

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.arellomobile.mvp.MvpDelegate

open class MvpAppCompatFragment : Fragment() {
    private var mIsStateSaved: Boolean = false

    private val mMvpDelegate = MvpDelegate(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mMvpDelegate.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        mIsStateSaved = false
        mMvpDelegate.onAttach()
    }

    override fun onResume() {
        super.onResume()
        mIsStateSaved = false
        mMvpDelegate.onAttach()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mIsStateSaved = true
        mMvpDelegate.onSaveInstanceState(outState)
        mMvpDelegate.onDetach()
    }

    override fun onStop() {
        super.onStop()
        mMvpDelegate.onDetach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mMvpDelegate.onDetach()
        mMvpDelegate.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (activity != null && activity!!.isFinishing) {
            mMvpDelegate.onDestroy()
            return
        }
        if (mIsStateSaved) {
            mIsStateSaved = false
            return
        }
        var anyParentIsRemoving = false
        var parent = parentFragment
        while (!anyParentIsRemoving && parent != null) {
            anyParentIsRemoving = parent.isRemoving
            parent = parent.parentFragment
        }

        if (isRemoving || anyParentIsRemoving) {
            mMvpDelegate.onDestroy()
        }
    }
}
