package com.gmail.victorkusov.mealbusket.view.adapter

import com.gmail.victorkusov.mealbusket.model.Product

interface AdapterController {

    fun setNewData(data:ArrayList<Product>)
}
