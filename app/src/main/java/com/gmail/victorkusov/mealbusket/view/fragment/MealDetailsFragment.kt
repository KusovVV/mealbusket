package com.gmail.victorkusov.mealbusket.view.fragment

import android.text.method.ScrollingMovementMethod
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.gmail.victorkusov.mealbusket.App
import com.gmail.victorkusov.mealbusket.R
import com.gmail.victorkusov.mealbusket.model.Product
import com.gmail.victorkusov.mealbusket.presentation.presenter.fragment.MealDetailsPresenter
import com.gmail.victorkusov.mealbusket.presentation.viewstate.fragment.MealDetailsView
import com.gmail.victorkusov.mealbusket.repository.MealRepository
import kotlinx.android.synthetic.main.fragment_meal_details.*
import javax.inject.Inject

class MealDetailsFragment :
    BaseFragment(),
    MealDetailsView {

    @Inject
    lateinit var repository: MealRepository

    @InjectPresenter
    lateinit var presenter: MealDetailsPresenter

    @ProvidePresenter
    fun providePresenter() =
        MealDetailsPresenter(repository)

    init {
        App.appComponent.inject(this)
    }

    override fun getLayoutResourceId() = R.layout.fragment_meal_details

    override fun viewData(data: Product?) {
        txtTitle.text = data?.name
        txtPrice.text = data?.price.toString()
        txtDescription.apply {
            text = data?.description ?: data?.decription
            if (lineCount > 3) {
                movementMethod = ScrollingMovementMethod()
            }
        }
        context?.let {
            Glide.with(it)
                .load(data?.image)
                .into(imgImage)
        }

        showBackIcon()
    }
}
