package com.gmail.victorkusov.mealbusket.view.adapter

interface ItemClickListener {
    fun onItemClick(id: String)
}
