package com.gmail.victorkusov.mealbusket.view.activity

import android.os.Bundle
import android.view.MenuItem
import com.arellomobile.mvp.presenter.InjectPresenter
import com.gmail.victorkusov.mealbusket.presentation.presenter.activity.MainPresenter
import com.gmail.victorkusov.mealbusket.presentation.viewstate.activity.MainView
import com.gmail.victorkusov.mealbusket.moxy.MvpAppCompatActivity
import com.gmail.victorkusov.mealbusket.R
import com.gmail.victorkusov.mealbusket.view.fragment.BaseFragment
import com.gmail.victorkusov.mealbusket.view.fragment.MealDetailsFragment
import com.gmail.victorkusov.mealbusket.view.fragment.MealListFragment

class MainActivity :
    MvpAppCompatActivity(),
    MainView,
    TransactionListener {
    @InjectPresenter
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showMealsFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(
                R.id.container,
                MealListFragment()
            )
            .commit()
    }

    override fun replace(fragment: BaseFragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }

    override fun showBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onBackPressed() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        super.onBackPressed()
    }
}
