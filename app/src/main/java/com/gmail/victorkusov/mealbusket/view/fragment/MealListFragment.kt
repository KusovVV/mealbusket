package com.gmail.victorkusov.mealbusket.view.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.victorkusov.mealbusket.*
import com.gmail.victorkusov.mealbusket.model.Product
import com.gmail.victorkusov.mealbusket.presentation.presenter.fragment.MealListPresenter
import com.gmail.victorkusov.mealbusket.presentation.viewstate.fragment.MealListView
import com.gmail.victorkusov.mealbusket.repository.MealRepository
import com.gmail.victorkusov.mealbusket.view.adapter.AdapterController
import com.gmail.victorkusov.mealbusket.view.adapter.ItemClickListener
import com.gmail.victorkusov.mealbusket.view.adapter.MealAdapter
import kotlinx.android.synthetic.main.fragment_meal_list.*
import javax.inject.Inject

class MealListFragment : BaseFragment(),
    MealListView {
    @Inject
    lateinit var repository: MealRepository

    @InjectPresenter
    lateinit var presenter: MealListPresenter

    @ProvidePresenter
    fun providePresenter() =
        MealListPresenter(repository)

    private val listener = object : ItemClickListener {
        override fun onItemClick(id: String) {
            presenter.itemClicked(id)
        }
    }

    private var adapterController: AdapterController? = null

    init {
        App.appComponent.inject(this)
    }


    override fun getLayoutResourceId() = R.layout.fragment_meal_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.apply {
            layoutManager = StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL)
            val mealAdapter = MealAdapter(listener)
            adapterController = mealAdapter
            adapter = mealAdapter
        }
    }

    override fun setupListData(data: ArrayList<Product>?) {
        if (data != null) {
            adapterController?.setNewData(data)
        }
    }
}