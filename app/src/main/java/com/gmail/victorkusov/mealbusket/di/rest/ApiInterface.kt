package com.gmail.victorkusov.mealbusket.di.rest

import com.gmail.victorkusov.mealbusket.model.Product
import com.gmail.victorkusov.mealbusket.model.ProductData
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("list")
    fun getMealList(): Observable<ProductData>

    @GET("{id}/detail")
    fun getMealDetailes(@Path("id") id: String): Single<Product>
}
