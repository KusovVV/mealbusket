package com.gmail.victorkusov.mealbusket.presentation.viewstate.fragment

import com.arellomobile.mvp.MvpView
import com.gmail.victorkusov.mealbusket.view.activity.Navigator

interface BaseView :MvpView, Navigator {
    fun showErrorToast()
}
