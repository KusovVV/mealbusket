package com.gmail.victorkusov.mealbusket.di

import com.gmail.victorkusov.mealbusket.di.rest.RestModule
import com.gmail.victorkusov.mealbusket.di.room.RoomModule
import com.gmail.victorkusov.mealbusket.repository.MealRepositoryModule
import com.gmail.victorkusov.mealbusket.view.fragment.MealDetailsFragment
import com.gmail.victorkusov.mealbusket.view.fragment.MealListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RestModule::class, RoomModule::class, MealRepositoryModule::class])
interface AppComponent {

    fun inject(fragment: MealListFragment)
    fun inject(fragment: MealDetailsFragment)
}