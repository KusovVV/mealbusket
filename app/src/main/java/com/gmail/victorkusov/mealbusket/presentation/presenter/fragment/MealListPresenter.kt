package com.gmail.victorkusov.mealbusket.presentation.presenter.fragment

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gmail.victorkusov.mealbusket.presentation.viewstate.fragment.MealListView
import com.gmail.victorkusov.mealbusket.repository.MealRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

@InjectViewState
class MealListPresenter(private val repository: MealRepository) : MvpPresenter<MealListView>() {

    private val compositeDisposable = CompositeDisposable()


    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        compositeDisposable.add(repository.getMealList()!!
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    viewState.setupListData(it)
                }, {
                    viewState.showErrorToast()
                }
            ))
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    fun itemClicked(id: String) {
        repository.setSelectedItemId(id)
        viewState.showMealDetails()
    }
}
