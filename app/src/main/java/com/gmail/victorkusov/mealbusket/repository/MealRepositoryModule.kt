package com.gmail.victorkusov.mealbusket.repository

import com.gmail.victorkusov.mealbusket.di.rest.ApiInterface
import com.gmail.victorkusov.mealbusket.di.room.RoomDao
import com.gmail.victorkusov.mealbusket.model.Product
import dagger.Module
import dagger.Provides
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class MealRepositoryModule {


    @Provides
    @Singleton
    fun provideRepository(apiInterface: ApiInterface,  room: RoomDao): MealRepository {
        return MealRepositoryImpl(apiInterface,room)
    }
}