package com.gmail.victorkusov.mealbusket.di.room

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule(private val context: Context) {

    @Provides
    @Singleton
    fun providesDatabase(): RoomDao {
        val database = Room.databaseBuilder(context, RoomClient::class.java, "RoomClient").build()
        database.roomDao()
        return database.roomDao()
    }
}