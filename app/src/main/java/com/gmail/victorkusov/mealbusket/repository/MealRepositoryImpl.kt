package com.gmail.victorkusov.mealbusket.repository

import com.gmail.victorkusov.mealbusket.di.rest.ApiInterface
import com.gmail.victorkusov.mealbusket.di.room.RoomDao
import com.gmail.victorkusov.mealbusket.model.Product
import dagger.Module
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

@Module
class MealRepositoryImpl(private val apiInterface: ApiInterface, private val room: RoomDao) : MealRepository {

    private var selectedId: String? = null

    override fun setSelectedItemId(id: String) {
        selectedId = id
    }

    override fun getMealList(): Single<ArrayList<Product>> {
        return apiInterface.getMealList()
            .subscribeOn(Schedulers.io())
            .map { it.data }
            .onErrorResumeNext(room.getMealList().map { ArrayList(it) })
            .firstOrError()
            .doAfterSuccess {
                it?.forEach(room::insertMeal)
            }
    }

    override fun getMeal(): Single<Product> {
        return apiInterface.getMealDetailes(selectedId!!)
            .subscribeOn(Schedulers.io())
            .onErrorResumeNext {
                room.getMeal(selectedId!!)
            }
            .doAfterSuccess {
                room.insertMeal(it)
            }
    }
}