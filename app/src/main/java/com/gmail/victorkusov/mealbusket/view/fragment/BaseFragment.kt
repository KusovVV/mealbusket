package com.gmail.victorkusov.mealbusket.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import com.gmail.victorkusov.mealbusket.moxy.MvpAppCompatFragment
import com.gmail.victorkusov.mealbusket.presentation.viewstate.fragment.BaseView
import com.gmail.victorkusov.mealbusket.view.activity.Navigator
import com.gmail.victorkusov.mealbusket.view.activity.TransactionListener

abstract class BaseFragment :
    MvpAppCompatFragment(),
    BaseView,
    Navigator {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(getLayoutResourceId(), container, false)

    @LayoutRes
    protected abstract fun getLayoutResourceId(): Int

    override fun showErrorToast() {
        Toast.makeText(context, "Network error", Toast.LENGTH_SHORT).show()
    }

    override fun showMealDetails() {
        transactionListener?.replace(MealDetailsFragment())
        showBackIcon()
    }

    protected fun showBackIcon() {
        transactionListener?.showBackButton()
    }

    private var transactionListener: TransactionListener? = null

    override fun onAttach(context: Context) {
        try {
            transactionListener = activity as TransactionListener
        } catch (e: ClassCastException) {
            throw ClassCastException("activity must implement TransactionListener")
        }
        super.onAttach(context)
    }

    override fun onDetach() {
        transactionListener = null
        super.onDetach()
    }
}
