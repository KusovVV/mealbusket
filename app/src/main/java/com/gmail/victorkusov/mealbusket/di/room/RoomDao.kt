package com.gmail.victorkusov.mealbusket.di.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gmail.victorkusov.mealbusket.model.Product
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface RoomDao {

    @Query("SELECT * From Product")
    fun getMealList(): Observable<List<Product>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMeal(meal: Product)

    @Query ("SELECT * From Product where id = :id")
    fun getMeal(id:String) : Single<Product>


    @Query("DELETE FROM Product")
    fun erase()

}