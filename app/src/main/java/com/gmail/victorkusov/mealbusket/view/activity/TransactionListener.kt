package com.gmail.victorkusov.mealbusket.view.activity

import com.gmail.victorkusov.mealbusket.view.fragment.BaseFragment

interface TransactionListener {

    fun replace(fragment: BaseFragment)
    fun showBackButton()
}
