package com.gmail.victorkusov.mealbusket.presentation.presenter.fragment

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gmail.victorkusov.mealbusket.presentation.viewstate.fragment.MealDetailsView
import com.gmail.victorkusov.mealbusket.repository.MealRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

@InjectViewState
class MealDetailsPresenter(private val repository: MealRepository) : MvpPresenter<MealDetailsView>() {

    private val compositeDisposable = CompositeDisposable()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        compositeDisposable.add(
            repository.getMeal()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        viewState.viewData(it)
                    }, onError = {
                        viewState.showErrorToast()
                    })

        )
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
}
