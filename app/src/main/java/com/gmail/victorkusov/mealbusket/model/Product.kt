package com.gmail.victorkusov.mealbusket.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity (tableName = "Product")
data class Product(
    @PrimaryKey
    @SerializedName("product_id")
    val id: String,
    @SerializedName("name")
    val name: String?,
    @SerializedName("price")
    val price: Int?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("decription")
    val decription: String?
)
