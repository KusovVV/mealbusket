package com.gmail.victorkusov.mealbusket.presentation.presenter.activity

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gmail.victorkusov.mealbusket.presentation.viewstate.activity.MainView

@InjectViewState
class MainPresenter :MvpPresenter<MainView>(){

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showMealsFragment()
    }
}
