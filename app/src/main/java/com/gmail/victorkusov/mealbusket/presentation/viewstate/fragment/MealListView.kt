package com.gmail.victorkusov.mealbusket.presentation.viewstate.fragment

import com.gmail.victorkusov.mealbusket.model.Product

interface MealListView : BaseView {
    fun setupListData(data: ArrayList<Product>?)
}
