package com.gmail.victorkusov.mealbusket.presentation.viewstate.fragment

import com.gmail.victorkusov.mealbusket.model.Product

interface MealDetailsView : BaseView {

    fun viewData(data: Product?)

}
