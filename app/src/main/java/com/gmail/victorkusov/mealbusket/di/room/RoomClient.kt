package com.gmail.victorkusov.mealbusket.di.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gmail.victorkusov.mealbusket.model.Product

@Database(entities = [Product::class], version = 1)
abstract class RoomClient : RoomDatabase() {

    abstract fun roomDao(): RoomDao
}