package com.gmail.victorkusov.mealbusket.model

import com.google.gson.annotations.SerializedName

data class ProductData (
    @SerializedName("products")
    val data:ArrayList<Product>
)
