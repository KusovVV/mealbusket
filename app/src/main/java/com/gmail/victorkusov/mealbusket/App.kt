package com.gmail.victorkusov.mealbusket

import android.app.Application
import com.gmail.victorkusov.mealbusket.di.AppComponent
import com.gmail.victorkusov.mealbusket.di.DaggerAppComponent
import com.gmail.victorkusov.mealbusket.di.rest.RestModule
import com.gmail.victorkusov.mealbusket.di.room.RoomModule
import com.gmail.victorkusov.mealbusket.repository.MealRepositoryModule

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .restModule(RestModule())
            .roomModule(RoomModule(applicationContext))
            .mealRepositoryModule(MealRepositoryModule())
            .build()
    }
}